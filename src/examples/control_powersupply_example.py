import labRemote
import argparse
import logging

logging.basicConfig()
logger = logging.getLogger(__name__)

def main(configFile, channelName):
    logger.debug(f"Settings:\nConfig file: {configFile}\nPS channel: {channelName}")
    hw = labRemote.ec.EquipConf()
    hw.setHardwareConfig(configFile)

    logger.debug("Configuring power-supply.")
    PS = hw.getPowerSupplyChannel(channelName)

    logger.debug("Program power-supply.")
    PS.program()

    logger.debug("Sending command to power-supply.")
    logger.info("Program")
    PS.program()
    logger.info("Turn ON")
    PS.turnOn()
    logger.info(f"Voltage: {PS.measureVoltage()}\nCurrent: {PS.measureCurrent()}")

    Agilent = PS.getPowerSupply()
    if not isinstance(Agilent, labRemote.ps.AgilentPs):
        logger.warning("The power supply is not an Agilent, can't execute the required action")
    else:
        logger.info("Switch off beeping for Agilent PS")
        Agilent.beepOff()

    logger.info("Turn OFF")
    PS.turnOff()

    logger.info("All done")
    return 0

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('configfile', type=str)
    parser.add_argument('channelname', type=str)
    parser.add_argument('-d','--debug', help='Enable more verbose printout, use multiple for increased debug level', action='store_true')
    args = parser.parse_args()

    if args.debug:
      logger.setLevel(logging.DEBUG)

    main(args.configfile, args.channelname)
