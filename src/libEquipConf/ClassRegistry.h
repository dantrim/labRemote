#ifndef LABREMOTE_CLASS_REGISTRY_H
#define LABREMOTE_CLASS_REGISTRY_H

#include <vector>
#include <functional>
#include <map>
#include <string>
#include <memory>

namespace EquipRegistry {

template <typename T, typename... A>
class ClassRegistry {

    typedef std::function<std::shared_ptr<T>(A... args)> FunctionType;
    typedef std::map<std::string, FunctionType> MapType;

    MapType registry;

 public:

    bool registerClass(const std::string& name,
                       FunctionType func) {
        registry[name] = func;
        return true;
    }

    std::shared_ptr<T> makeClass(const std::string& name, A... args) {
        try {
            return registry.at(name)(args...);
        } catch(std::out_of_range &e) {
            return nullptr;
        }
    }

    std::vector<std::string> listClasses() {
        std::vector<std::string> known;
        for (auto &i: registry) {
            known.push_back(i.first);
        }
        return known;
    }
};

} //namespace EquipRegistry

#endif
