#include "EquipConf.h"

#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>

#include "PowerSupplyRegistry.h"
#include "ComRegistry.h"

////////////////////
// Configuration
////////////////////

EquipConf::EquipConf()
{
}

EquipConf::~EquipConf()
{
}

EquipConf::EquipConf(const std::string& hardwareConfigFile)
{
  setHardwareConfig(hardwareConfigFile);
}

EquipConf::EquipConf(const json& hardwareConfig)
{
  setHardwareConfig(hardwareConfig);
}

void EquipConf::setHardwareConfig(const std::string& hardwareConfigFile)
{
  //load JSON object from file
  std::ifstream i(hardwareConfigFile);
  i >> m_hardwareConfig;
}

void EquipConf::setHardwareConfig(const json& hardwareConfig)
{
  //store JSON config file
  m_hardwareConfig = hardwareConfig;
}

json EquipConf::getDeviceConf(const std::string& label)
{
  for (const auto& hw : m_hardwareConfig["devices"].items()) {
    //check label 
    if (hw.key() == label) return hw.value();
  }
  return json();
}

json EquipConf::getChannelConf(const std::string& label)
{
  for (const auto& ch : m_hardwareConfig["channels"].items()) {
    //check label 
    if (ch.key() == label) return ch.value();
  }
  return json();
}

////////////////////
// General private
////////////////////


////////////////////
// Power-Supply
////////////////////

std::shared_ptr<IPowerSupply> EquipConf::getPowerSupply(const std::string& name)
{
  //first check if an object with the same name/type is already available (was already instantiated)
  if (m_listPowerSupply.find(name) != m_listPowerSupply.end()) {
    return m_listPowerSupply[name];
  }

  //Otherwise, create the object
  //check first if hardware configuration is available
  json reqPSConf = getDeviceConf(name);
  if (reqPSConf.empty()) {
    std::cerr << "Requested device not found in input configuration file: " << name << std::endl;
    return nullptr;
  }
  if (reqPSConf["hw-type"] != "PS") {
    std::cerr << "Requested power supply device has wrong hw-type field in configuration." << name << ", type = " << reqPSConf["hw-type"] << std::endl;
    return nullptr;    
  }

  //setup device
  std::shared_ptr<IPowerSupply> ps = EquipRegistry::createPowerSupply(reqPSConf["hw-model"], name);

  // configure already the PS
  ps->setConfiguration(reqPSConf);

  // Setup communication object
  if(!reqPSConf.contains("communication"))
    {
      std::cerr << "Requested device is missing communication configuration" << std::endl;
      return nullptr;
    }
  ps->setCom(createCommunication(reqPSConf["communication"]));

  // Check
  ps->checkCompatibilityList();

  return ps;
}

std::shared_ptr<PowerSupplyChannel> EquipConf::getPowerSupplyChannel(const std::string& name)
{
  //first check if an object with the same name/type is already available (was already instantiated)
  if (m_listPowerSupplyChannel.find(name) != m_listPowerSupplyChannel.end())
    return m_listPowerSupplyChannel[name];

  //Otherwise, create the object
  //check first if hardware configuration is available
  json reqChConf = getChannelConf(name);
  if (reqChConf.empty())
    {
      std::cerr << "Requested channel not found in input configuration file: " << name << std::endl;
      return nullptr;
    }

  if (reqChConf["hw-type"] != "PS")
    {
      std::cerr << "Requested power supply channel has wrong hw-type field in configuration." << name << ", type = " << reqChConf["hw-type"] << std::endl;
      return nullptr;
    }

  // Get physical power supply
  std::shared_ptr<IPowerSupply> ps = getPowerSupply(reqChConf["device"]);
  if(ps==nullptr)
    {
      std::cerr << "Cannot fetch requested power supply: " <<  reqChConf["device"] << std::endl;
      return nullptr;
    }    

  // Other settings
  unsigned ch = reqChConf["channel"];

  // Create!
  std::shared_ptr<PowerSupplyChannel> theChannel = std::make_shared<PowerSupplyChannel>(name, ps, ch);

  //configure already the PS
  if(reqChConf.contains("program"))
    theChannel->setProgram(reqChConf["program"]);

  return theChannel;
}

std::shared_ptr<ICom> EquipConf::createCommunication(const json& config) const
{
  if (!config.contains("protocol"))
    throw std::runtime_error("Communicaiton block is missing protocol definition.");

  std::shared_ptr<ICom> com=EquipRegistry::createCom(config["protocol"]);
  com->setConfiguration(config);
  com->init();

  return com;
}
