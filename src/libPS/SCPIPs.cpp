#include "SCPIPs.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(SCPIPs)

SCPIPs::SCPIPs(const std::string& name, std::vector<std::string> models,  unsigned maxChannels) :
IPowerSupply(name, models), m_maxChannels(maxChannels)
{ }

bool SCPIPs::ping()
{
  std::string result = m_com->sendreceive("*IDN?");
  return !result.empty();
}

void SCPIPs::reset()
{
  m_com->send("*RST");

  if(!ping())
    throw std::runtime_error("No communication after reset.");
}

std::string SCPIPs::identify()
{
  std::string idn=m_com->sendreceive("*IDN?");
  return idn;
}

void SCPIPs::turnOn(unsigned channel)
{
  setChannel(channel);
  m_com->send("OUTPUT ON");
}

void SCPIPs::turnOff(unsigned channel)
{
  setChannel(channel);
  m_com->send("OUTPUT OFF");
}

void SCPIPs::setChannel(unsigned channel)
{
  if(m_maxChannels>0)
    { // In-range channel check
      if(channel>m_maxChannels)
	throw std::runtime_error("Invalid channel: "+std::to_string(channel));
    }

  if(m_maxChannels==1) return; // No need to change channel

  m_com->send("INST:NSEL " + std::to_string(channel));
}

void SCPIPs::setCurrentLevel(double cur, unsigned channel)
{
  setChannel(channel);
  m_com->send("CURRENT " + std::to_string(cur));
}

double SCPIPs::getCurrentLevel(unsigned channel)
{
  setChannel(channel);
  return std::stod(m_com->sendreceive("CURRENT?"));
}

void SCPIPs::setCurrentProtect(double maxcur, unsigned channel)
{
  setChannel(channel);
  m_com->send("CURRENT " + std::to_string(maxcur));
}

double SCPIPs::getCurrentProtect(unsigned channel)
{
  return getCurrentLevel();
}

double SCPIPs::measureCurrent(unsigned channel)
{
  setChannel(channel);
  return std::stod(m_com->sendreceive("MEAS:CURR?"));
}

void SCPIPs::setVoltageLevel(double volt, unsigned channel)
{
  setChannel(channel);
  m_com->send("VOLTAGE " + std::to_string(volt));
}

double SCPIPs::getVoltageLevel(unsigned channel)
{
  setChannel(channel);
  return std::stod(m_com->sendreceive("VOLTAGE?"));
}

void SCPIPs::setVoltageProtect(double maxvolt, unsigned channel)
{
  setChannel(channel);
  m_com->send("VOLTAGE " + std::to_string(maxvolt));
}

double SCPIPs::getVoltageProtect(unsigned channel)
{
  return getVoltageLevel();
}

double SCPIPs::measureVoltage(unsigned channel)
{
  setChannel(channel);
  return std::stod(m_com->sendreceive("MEAS:VOLT?"));
}


