#include "RS_HMPXXXX.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(RS_HMPXXXX)

RS_HMPXXXX::RS_HMPXXXX(const std::string& name) :
SCPIPs(name, {"HMP4040"})
{ }

