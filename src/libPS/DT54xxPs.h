#ifndef DT54XXPS_H
#define DT54XXPS_H


#include <chrono>
#include <memory>
#include <string>

#include "IPowerSupply.h"

/** \brief CAEN DT54xx
 * CAEN DT54xx USB High Voltage Power Supplies
 *
 * [Programming Manual](https://www.caen.it/products/dt5472/)
 *
 * Assumes direct USB connection.
 */
class DT54xxPs : public IPowerSupply
{
public:
  /**
   * Interpretation of status bits.
   * The value is the bitmask to select the bit in status() return value.
   */
  enum Status
    {
      On         =(1<< 0), // 1 : ON 0 : OFF
      RampingUp  =(1<< 1), // 1 : Channel Ramping UP
      RampingDown=(1<< 2), // 1 : Channel Ramping DOWN
      OVC        =(1<< 3), // 1 : Over current
      OVV        =(1<< 4), // 1 : Over voltage
      UNV        =(1<< 5), // 1 : Under voltage
      MAXV       =(1<< 6), // 1 : VOUT in MAXV protection
      Trip       =(1<< 7), // 1 : Current generator
      OVT        =(1<< 8), // 1 : Over temperature
      Disabled   =(1<<10), // 1 : Ch disabled
      Kill       =(1<<11), // 1 : Ch in KILL
      Interlock  =(1<<12), // 1 : Ch in INTERLOCK
      CalError   =(1<<13)  // 1 : Calibration Error
    };

  DT54xxPs(const std::string& name);
  ~DT54xxPs() =default;

  /** \name Communication
   * @{
   */

  virtual bool ping();

  virtual std::string identify();

  /** @} */

  /** \name Power Supply Control
   * @{
   */

  virtual void reset();

  /** \brief Turn on power supply
   * 
   * Block until power supply finishes ramping.
   *
   * @param channel channel, if any
   */
  virtual void turnOn(unsigned channel);

  /** \brief Turn off power supply
   *
   * Block until power supply finishes rampdown.
   * @param channel channel, if any
   */
  virtual void turnOff(unsigned channel);

  /** @} */
  
  /** \name Current Control and Measurement
   * @{
   */

  virtual void   setCurrentLevel(double cur, unsigned channel = 0);
  virtual double getCurrentLevel(unsigned channel = 0);
  virtual void   setCurrentProtect(double maxcur , unsigned channel = 0);
  virtual double getCurrentProtect(unsigned channel = 0);  
  virtual double measureCurrent(unsigned channel = 0);

  /** @} */

  /** \name Voltage Control and Measurement
   * @{
   */

  virtual void   setVoltageLevel(double volt, unsigned channel = 0);
  virtual double getVoltageLevel(unsigned channel = 0);
  virtual void   setVoltageProtect(double maxvolt , unsigned channel = 0 );
  virtual double getVoltageProtect(unsigned channel = 0);
  virtual double measureVoltage(unsigned channel = 0);

  /** @} */

  /** \name Model-specific functionality
   * @{
   */

  /**
   * Return the status of the Power Supply.
   * Use with Status enum to interpret bits.
   *
   * @param channel Channel to query
   *
   * @return status bits
   */
  uint16_t status(unsigned channel = 0);

  /** @} */

private:
  /**
   * Build a command string and parse the response
   *
   * Throws an exception if any of the following errors are detected:
   * - no response 
   * - returned CMD value is ERR
   *
   * \param cmd CMD value
   * \param par PAR value
   * \param value VAL value (if empty, not appended)
   *
   * \return The returned VAL value.
   */
  std::string command(const std::string& cmd, const std::string& par, const std::string& value="");
};

#endif // DT54XXPS_H

