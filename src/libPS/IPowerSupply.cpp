#include "IPowerSupply.h"

#include <iostream>
#include <stdexcept>
#include "Logger.h"

IPowerSupply::IPowerSupply(const std::string& name, std::vector<std::string> models)
{
  m_name = name;
  m_models = models;
}

void IPowerSupply::setCom(std::shared_ptr<ICom> com)
{
  if(!com->is_open())
    com->init();

  m_com = com;
  if(!ping())
    throw std::runtime_error("Failed communication with the PS");
}

void IPowerSupply::setConfiguration(const nlohmann::json& config)
{  
  m_config = config;
}

const nlohmann::json& IPowerSupply::getConfiguration() const
{
  return m_config;
}

void IPowerSupply::checkCompatibilityList()
{
  // get model connected to the PS
  std::string idn = identify();

  // get list of models
  std::vector<std::string> models = IPowerSupply::getListOfModels();

  if (models.empty()){
    logger(logINFO) << "No model identifier implemented for this power supply. No check is performed.";
    return;
  }

  for(const std::string& model : models)
    {
      if(idn.find(model) != std::string::npos)
	return;
    }
  
  logger(logERROR)<< "Unknown power supply: " << idn;
  throw std::runtime_error("Unknown power supply: " + idn);

}

std::vector<std::string> IPowerSupply::getListOfModels()
{
  return m_models;
}

void IPowerSupply::setCurrentProtect(double cur, unsigned channel)
{
  logger(logWARNING) << "setCurrentProtect() not implemented for this PS.";
}

double IPowerSupply::getCurrentProtect(unsigned channel)
{
  logger(logWARNING) << "getCurrentProtect() not implemented for this PS.";
  return 0;
}

void IPowerSupply::setVoltageProtect(double volt, unsigned channel)
{
  logger(logWARNING) << "setVoltageProtect() not implemented for this PS.";
}

double IPowerSupply::getVoltageProtect(unsigned channel)
{
  logger(logWARNING) << "getVoltageProtect() not implemented for this PS.";
  return 0;
}
