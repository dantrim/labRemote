#ifndef POWERSUPPLYCHANNEL_H
#define POWERSUPPLYCHANNEL_H

#include <string>
#include <nlohmann/json.hpp>

#include "IPowerSupply.h"

/** A single channel of a power supply.
 *
 * Represents a single channel of a physical, potentially multi-channel, 
 * power supply (IPowerSupply).
 */ 
class PowerSupplyChannel
{
public:
  /** Create object corresponding to a single channel of a power supply.
   *
   * No commands are called here.
   *
   * @param name Name of the channel
   * @param ps Reference to the physical power supply
   * @param channel Channel number
   */
  PowerSupplyChannel(const std::string& name, std::shared_ptr<IPowerSupply> ps, unsigned channel);

  /** Get name of channel
   * @return channel name
   */
  std::string getName() const;

  /** Get pointer to the underlaying physical power supply
   * @return power supply pointer
   */
  std::shared_ptr<IPowerSupply> getPowerSupply() const;

  /** Get physical channel in the power supply
   * @return channel number in the device
   */
  unsigned getChannel() const;
  
  /** Settings to set on program.
   *
   * Commands (keys) map to
   *  maxvoltage -> setVoltageProtect
   *  maxcurrent -> setCurrentProtect
   *  voltage -> setVoltageLevel
   *  current -> setCurrentLevel
   */
  void setProgram(const nlohmann::json& settings);

  /** Program the device according to the JSON configuration [optional] */
  void program();

  /** \name Power Supply Control
   * @{
   */
  
  /** Turn on power supply (channel)
   *
   * Warning: Not all power supplies support per-channel on/off control.
   */  
  void turnOn();

  /** Turn off power supply 
   *
   * Warning: Not all power supplies support per-channel on/off control.
   */
  void turnOff();

  /** @} */

  /** \name Current Control and Measurement
   * @{
   */

  /** Set current of PS
   * @param cur current [A]
   */
  void setCurrentLevel(double cur);

  /** Get current of PS
   * @return current level [A]
   */
  double getCurrentLevel();
  
  /** Set current protection
   * @param cur maximum current [A]
   */
  void setCurrentProtect(double cur);

  /** Measure current of PS
   * @return current, convert to [A]
   */
  double measureCurrent();

  /** @} */

  /** \name Voltage Control and Measurement
   * @{
   */

  /** Set voltage of PS       
   * @param volt voltage [V]
   */
  void setVoltageLevel(double volt);

  /** Get voltage level of PS
   * @return voltage level [V]
   */
  double getVoltageLevel();
  
  /** Set current protection [optional]
   * @param volt maximum current [A]
   */
  void setVoltageProtect(double volt);  

  /** Measure voltage of PS.
   * @return voltage, convert to [V]
   */
  double measureVoltage();

  /** @} */

private:
  /** Store name of the channel */
  std::string m_name;

  /** Program to apply on program() */
  nlohmann::json m_program;

  /** Reference to the physical power supply */
  std::shared_ptr<IPowerSupply> m_ps=nullptr;

  /** Channel number */
  unsigned m_channel;
};

#endif // POWERSUPPLYCHANNEL_H
