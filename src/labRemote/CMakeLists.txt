file(GLOB REGISTER_PYTHON_MODULE_SRC
          CONFIGURE_DEPENDS
          "${PROJECT_SOURCE_DIR}/src/*/python.cpp"
          )

pybind11_add_module(labRemote module.cpp ${REGISTER_PYTHON_MODULE_SRC})
target_link_libraries(labRemote PRIVATE Com Utils PS EquipConf pybind11_json)


