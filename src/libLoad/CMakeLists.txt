add_library(Load SHARED)
target_sources(Load
  PRIVATE
  Bk85xx.cpp
  TTILD400P.cpp
  )
target_link_libraries(Load PRIVATE Com Utils)
target_include_directories(Load PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
