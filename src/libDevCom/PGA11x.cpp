#include "PGA11x.h"

PGA11x::PGA11x(std::shared_ptr<SPICom> com)
  : m_com(com)
{ }

void PGA11x::select(uint32_t ch)
{
  m_com->write_reg16(0x2A00 | ch&0xF);
}
