#include "ChecksumException.h"

#include <sstream>
#include <iomanip>

ChecksumException::ChecksumException(uint32_t checksum)
{
  std::stringstream ss;
  ss << "0x" << std::hex << std::setfill('0') << std::setw(2) << checksum << std::endl;
  m_msg="Checksum: "+ss.str();
 }

const char* ChecksumException::what() const throw()
{ return m_msg.c_str(); }
