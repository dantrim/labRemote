#ifndef SHT85_H
#define SHT85_H

#include "ClimateSensor.h"
#include "I2CCom.h"

#include <memory>

class SHT85 : public ClimateSensor
{
public:
  SHT85(std::shared_ptr<I2CCom> i2c);
  virtual ~SHT85();

  virtual void init();
  virtual void reset();
  virtual void read();

  virtual unsigned status() const;
  virtual float temperature() const;
  virtual float humidity() const;
  virtual float pressure() const;

private:
  std::shared_ptr<I2CCom> m_i2c;

  int m_status;
  float m_temperature;
  float m_humidity;

  uint8_t calcCRC(uint8_t byte0,uint8_t byte1,uint8_t crc) const;
};

#endif // SHT85_H
