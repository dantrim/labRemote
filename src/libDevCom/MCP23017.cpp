#include "MCP23017.h"

#include <iostream>
#include <iomanip>

MCP23017::MCP23017(std::shared_ptr<I2CCom> com)
  : IOExpander(), m_com(com)
{
  m_com->write_reg8(MCP23017_IPOLA, 0x0); // Do not support inversion
  m_com->write_reg8(MCP23017_IPOLB, 0x0); // Do not support inversion
}

uint32_t MCP23017::getIO()
{ 
  uint16_t ioa=m_com->read_reg8(MCP23017_IODIRA);
  uint16_t iob=m_com->read_reg8(MCP23017_IODIRB);
  return (iob<<8)|(ioa<<0);
}

void MCP23017::setIO(uint32_t output)
{
  m_com->write_reg8(MCP23017_IODIRA, (output>>0)&0xFF);
  m_com->write_reg8(MCP23017_IODIRB, (output>>8)&0xFF);
}

void MCP23017::write(uint32_t value)
{
  m_com->write_reg8(MCP23017_GPIOA, (value>>0)&0xFF);
  m_com->write_reg8(MCP23017_GPIOB, (value>>8)&0xFF);
}

uint32_t MCP23017::read()
{ 
  uint16_t reada=m_com->read_reg8(MCP23017_OLATA);
  uint16_t readb=m_com->read_reg8(MCP23017_OLATB);
  return (readb<<8)|(reada<<0);
}

