#include "SHT85.h"

#include <unistd.h>
#include <math.h>

#include "NotSupportedException.h"
#include "ChecksumException.h"

SHT85::SHT85(std::shared_ptr<I2CCom> i2c)
  : m_i2c(i2c)
{ }

SHT85::~SHT85()
{ }

void SHT85::init()
{ }

void SHT85::reset()
{ }

void SHT85::read()
{
  m_i2c->write_reg16(0x2400);
  usleep(1000e3);

  std::vector<uint8_t> data(6);
  m_i2c->read_block(data);

  uint16_t tdata=((data[0])<<8)|data[1];
  uint8_t tcrc=calcCRC(data[0],data[1],data[2]);
  if(tcrc)
    throw ChecksumException(tcrc);

  uint16_t hdata=((data[3])<<8)|data[4];
  uint8_t hcrc=calcCRC(data[3],data[4],data[5]);
  if(hcrc)
    throw ChecksumException(hcrc);

  // Parse the data
  m_humidity=100*((float)hdata)/(pow(2,16)-1.);
  m_temperature=-45+175*((float)tdata)/(pow(2,16)-1.);
}

uint SHT85::status() const
{ return m_status; }

float SHT85::temperature() const
{ return m_temperature; }

float SHT85::humidity() const
{ return m_humidity; }

float SHT85::pressure() const
{ throw NotSupportedException("SHT85 does not have a pressure sensor"); return 0; }

uint8_t SHT85::calcCRC(uint8_t byte0,uint8_t byte1,uint8_t crc) const
{
  static const uint8_t poly=0x31;
  uint16_t reg=0xFF^byte0;

  uint8_t bytes[]={byte1, crc};
  for(uint32_t b=0;b<2;b++)
    {
      for(uint32_t bit=0;bit<8;bit++)
	{
	  // shift the bit in
	  reg=(reg<<1)|(((bytes[b]>>(7-bit)))&1);
	  if(reg&0x100)
	    reg^=poly;
	}
    }

  return (reg&0xFF);
}
