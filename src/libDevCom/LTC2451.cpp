#include "LTC2451.h"

#include "LinearCalibration.h"

#include "NotSupportedException.h"
#include "OutOfRangeException.h"

#include <thread>
#include <chrono>

LTC2451::LTC2451(double reference, std::shared_ptr<I2CCom> com)
  : ADCDevice(std::make_shared<LinearCalibration>(reference, 0xFFFF)),
    m_com(com)
{
  setSpeed(Speed30Hz);
}

LTC2451::~LTC2451()
{ }

int32_t LTC2451::readCount()
{
  m_com->read_reg16(); // dummy read to start conversion cycle
  std::this_thread::sleep_for(std::chrono::milliseconds(1000/30+1)); // assume 30Hz rate
  return m_com->read_reg16();
}

int32_t LTC2451::readCount(uint8_t ch)
{
  if(ch!=0)
    throw OutOfRangeException(ch,0,0);
  return readCount();
}

void LTC2451::readCount(const std::vector<uint8_t>& chs, std::vector<int32_t>& counts)
{
  counts.clear();
  for(uint8_t ch : chs)
    counts.push_back(readCount(ch));
}

void LTC2451::setSpeed(Speed speed)
{
  m_com->write_reg8(speed);
}
