# Guidelines for contributing to labRemote

__Project Manager:__ Elisabetta Pianori
__Maintainers:__
- Timon Heim
- Simone Pagan Griso
- Karol Krizka
- Elisabetta Pianori

## Definition of Roles

The _Project Manager_'s role is to keep an overview of the development happening in labRemote. The PM's responsibilities include:
- being the main contact for labRemote
- regularly merging the `devel` branch into `master`
- merging all Merge Requests

The _Maintainers_ have a very good knowledge of the labRemote code base and are responsible for reviewing Merge Requests.

## Usage of The devel Branch
The `master` branch is to be assumed as relatively stable. All new development should be merged into the `devel` branch. The `devel` branch is __not__ to be used for applications!

The `devel` branch will be merged into `master` every Monday, with weeks skipped based on the Project Manager's discretion.

## Merge Requests
If working on a new feature, start a _WIP Merge Request_ as soon as possible . This lets everyone know that you have started the work and prevents duplication of work.

Once you are happy with the state of the work, remove the _WIP_ flag. This will let the Project Manager and Maintainers know that the code review process should begin.

The Merge Request will be merged by the Project Manager.

### Review of Merge Requests
All merge requests must be approved by at least two Maintainers; one of them being the Project Manager and neither being the person initiating the MR. 

All discussions need to be marked as resolved before a MR can proceed. It is the job of the person initiating the discussion to mark a discussion as resolved.