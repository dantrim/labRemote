# Small library to control and read out lab equipment.

## Getting code

If you want the external dependencies compiled internally, then make sure you clone recursive:

```
git clone --recursive <url>
```

## Commiting code
Create new branch with your initials and short description, for example `jd_mybranch`. When creating merge request make sure to check the box with delete branch after merging.

## Compile code
To start with:
```shell
mkdir build; cd build
cmake .. # On CentOS, use the cmake3 command
make -j4
```

## New projects
You can add labRemote as a submodule and create a new project.
All projects should be [labRemote-apps](https://gitlab.cern.ch/berkeleylab/labremote-apps)
Example project is:
[ExampleLabRemoteProject](https://gitlab.cern.ch/berkeleylab/labremote-apps/examplelabremoteproject)

## Probe Station

The external dependencies listed below are for various pieces of functionality in the project.

Requirements for probe station controller:
 * Qt 5.9.2 [link](https://www.qt.io/download-qt-for-application-development)
 * OpenCV 3.3.1 [Install in Linux](https://docs.opencv.org/3.3.1/d7/d9f/tutorial_linux_install.html)
 * gclib [link](http://galilmc.com/sw/pub/all/doc/gclib/html/osx.html)

Requirements for FTDICom:
 * [libftdi](https://www.intra2net.com/en/developer/libftdi/download.php) - Generic FTDI library
 * [libmpsse](https://github.com/l29ah/libmpsse) - Library that implements common communication protocols (I2C, SPI) using the MPSSE framework on FTDI chips

Requirements for JSON (used by libPS and libEquipConf):
 * [nlohmann_json](https://github.com/nlohmann/json) - header-only JSON for Modern C++

# Python Bindings

Python bindings are built via the `labRemote` target which is enabled by the cmake flag `-DUSE_PYTHON=on`. This will put a `.so` file in `${BUILD}/lib/` that can be directly used in python. As such, it requires the Python libraries to be findable (e.g. `python3-devel` is installed). For example, after building with `make`, while inside the build directory,

```
PYTHONPATH=lib/:$PYTHONPATH python3
Python 3.6.8 (default, Aug  7 2019, 17:28:10)
[GCC 4.8.5 20150623 (Red Hat 4.8.5-39)] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import labRemote
```

For now, the following classes are supported:

  - libCom
    - ICom
      - CharDeviceCom
      - SerialCom
        - TextSerialCom
        - GPIBSerialCom
  - libEquipConf
    - EquipConf
  - libPS
    - IPowerSupply
      - AgilentPs
        - AgilentE3631APs
        - AgilentE364xAPs
        - AgilentE3634APs
      - Bk16XXPs
      - DT54xxPs
      - Keithley22XX
      - Keithley24XX
      - RS_HMPXXXX
      - RigolDP832
      - SorensenPs
      - TTIMX180TPPs
      - TTITSX1820PPs
    - PowerSupplyChannel

## Requirements/Installing

Note that in every shell, python needs to be able to find the built shared-object files to dynamically load the python bindings. Make sure you export the `PYTHONPATH` as shown below and/or described in the `make` output.

```
yum install python3-devel
cmake3 -DUSE_PYTHON=on
make
export PYTHONPATH=/home/<username>/<directoryToLabRemote>/build/lib:$PYTHONPATH
python3 -c "import labRemote"
```
